/*
*/

#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <signal.h>
#include <pthread.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <linux/unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <stdbool.h>

#include "datagram.h"


#define gettid() syscall(__NR_gettid)
#define sigev_notify_thread_id _sigev_un._tid

#define PRIO 80
#define POLICY SCHED_FIFO
//#define POLICY SCHED_RR
//#define POLICY SCHED_OTHER
#define CLOCK CLOCK_MONOTONIC

#define MS_TO_NANOS 1000000LL
#define SUPER_FRAME   (100 * MS_TO_NANOS)
#define BEACON_WINDOW ( 20 * MS_TO_NANOS)
#define SAFETY_DELAY  (  4 * MS_TO_NANOS)
#define SLOT_WINDOW   (  4 * MS_TO_NANOS)
#define SLOT_TIME(slotNumber) (BEACON_WINDOW + SAFETY_DELAY + SLOT_WINDOW * (slotNumber) - SLOT_WINDOW/2)

void createTimer(timer_t * timer, int signal);
void setTimer(timer_t timer, uint64_t time);
uint64_t now();
void printUsage();
void parseInputParameters(int argc, char** argv,  const char ** mcastAddress, int * port, int * slot, const char ** customHostname);
bool processMessage(const char * buf);
void awaitTimer(sigset_t * sigset);
void sendBeacon();
void sendSlotMessage();


unsigned int frameCounter = 0;
unsigned int lastFrameCounter = 0;
uint64_t superframeStartTime;

char buf[1024];
char buftmp[1024];
char output[1024];

timer_t timer;

int socketId;

//Differenz zwischen der realen Zeit und der synchronisierten Anwendungszeit.
//Die synchronisierte Anwendungszeit ergibt sich aus der Beaconnummer.
//Sie wird gerechnet vom Startzeitpunkt des Superframes mit der Beaconnummer 0
uint64_t timeOffset = 0;

int port;
int slot;
const char * mcastAddress;
char hostname[128];


/*
 *
 */
int main(int argc, char** argv) {

	const char * customHostname;
	parseInputParameters(argc, argv, &mcastAddress, &port, &slot, &customHostname);

	//Initialisiere Socket.
	//Trete der Multicast-Gruppe bei
	//Aktiviere Signal SIGIO
	socketId = initSocket(mcastAddress, port);
	if (socketId < 0) {
		exit(1);
	}

	// if no hostname manually set, get real one
	if (customHostname == NULL && (gethostname(hostname, sizeof(hostname)) < 0)) {
		perror("hostname");
		exit(-1);
	} else {
		strncpy(hostname, customHostname, sizeof(hostname));
	}

	//Definiere Ereignis fuer den Timer
	//Beim Ablaufen des Timers soll das Signal SIGALRM
	//an die aktuelle Thread gesendet werden.
	createTimer(&timer, SIGALRM);

	struct sched_param schedp;

	//Umschaltung auf Real-time Scheduler.
	//Erfordert besondere Privilegien.
	//Deshalb hier deaktiviert.
	/*
	memset(&schedp, 0, sizeof (schedp));
	schedp.sched_priority = PRIO;
	sched_setscheduler(0, POLICY, &schedp);
	*/


	//Lege fest, auf welche Signale beim
	//Aufruf von sigwaitinfo gewartet werden soll.
	sigset_t sigset;
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGIO);                  //Socket hat Datagramme empfangen
	sigaddset(&sigset, SIGALRM);                //Timer ist abgelaufen
	sigaddset(&sigset, SIGINT);                 //Cntrl-C wurde gedrueckt
	sigprocmask(SIG_BLOCK, &sigset, NULL);

	superframeStartTime = now();

	//Merker fuer Programmende
	int finished = 0;
	while (finished == 0) {
		// warte für zufällige Zeit innerhalb des Beacon-Zeitfensters
		setTimer(timer, superframeStartTime + randomNumber(BEACON_WINDOW));
		awaitTimer(&sigset);

		// Nachrichten abarbeiten
		char* sourceaddr;
		int sourceport;
		bool beaconReceived = false;

		while (recvMessage(socketId, buf, sizeof(buf), &sourceaddr, &sourceport) > 0) {
			beaconReceived |= processMessage(buf);
		}

		if (!beaconReceived) {
			sendBeacon();
		}

		// Auf Slot warten
		setTimer(timer, superframeStartTime + SLOT_TIME(slot));
		awaitTimer(&sigset);
		sendSlotMessage();

		superframeStartTime = superframeStartTime + SUPER_FRAME;
		frameCounter++;
	}

	////////////////////////////////////////////////////


	//und aufraeumen
	timer_delete(timer);


	/* switch to normal */
	schedp.sched_priority = 0;
	sched_setscheduler(0, SCHED_OTHER, &schedp);

	return 0;
}

void printUsage() {
	printf("Todo: write usage.\n");
}

void parseInputParameters(int argc, char** argv,  const char ** mcastAddress, int * port, int * slot, const char ** customHostname) {
	*mcastAddress = NULL;
	*port = 0;
	*slot = 0;
	*customHostname = NULL;

	char c;
	while ((c = getopt(argc, argv, "p:a:s:h:")) != -1) {
		switch (c) {
			case 'p':
				*port = atoi(optarg);
				break;
			case 'a':
				*mcastAddress = optarg;
				break;
			case 's':
				*slot = atoi(optarg);
				break;
			case 'h':
				*customHostname = optarg;
		}
	}

	if (*port == 0 || *mcastAddress == NULL) {
		printUsage();
		exit(-1);
	}
	if (*slot < 1 || *slot > 16) {
		printf("Invalid slot number: %d\n" , *slot);
		exit(-1);
	}
	printf("%s:%d, Slot %d\n", *mcastAddress, *port, *slot);
}

void createTimer(timer_t * timer, int signal) {
	struct sigevent sigev;
	sigev.sigev_notify = SIGEV_THREAD_ID | SIGEV_SIGNAL;
	sigev.sigev_signo = signal;
	sigev.sigev_notify_thread_id = gettid();

	//Erzeuge den Timer
	timer_create(CLOCK, &sigev, timer);
}

void setTimer(timer_t timer, uint64_t time) {
	struct itimerspec tspec;
	tspec.it_interval.tv_sec = 0;
	tspec.it_interval.tv_nsec = 0;

	nsec2timespec(&tspec.it_value, time);
	timer_settime(timer, TIMER_ABSTIME, &tspec, NULL);
}

uint64_t now() {
	struct timespec now;
	clock_gettime(CLOCK, &now);
	return timespec2nsec(&now);
}

bool processMessage(const char * buf) {
	bool beaconReceived = false;

	if (buf[0] == 'B') {
		beaconReceived = true;

		//Empfangenes Datagram ist ein Beacon
		uint32_t beaconDelay;
		char senderHostname[128];

		unsigned int receivedFrameCounter;
		int rc = decodeBeacon(buf, &receivedFrameCounter, &beaconDelay, senderHostname, sizeof(senderHostname));
		if (rc < 0) {
			printf("### Invalid Beacon: '%s'\n", buf);
			return false;
		}

		if (strcmp(hostname, senderHostname) == 0) {
			// We ignore our own messages
			return false;
		}

		if (receivedFrameCounter < frameCounter) {
			printf("Wrong Counter received: %d (expected %d)\n", receivedFrameCounter, frameCounter);
			return false;
		} else if (receivedFrameCounter > frameCounter) {
			printf("Warning: frame(s) skipped: %d\n", receivedFrameCounter - frameCounter);
			frameCounter = receivedFrameCounter;
		}


		// Berechne unsere Erwartung an die Superframe-Startzeit
		//superframeStartTime = superframeStartTime + SUPER_FRAME;

		//Berechne den Zeitpunkt, an dem der Superframe begann
		uint64_t receivedSuperFrameStartTime = now() - beaconDelay;

		if (receivedSuperFrameStartTime != superframeStartTime) {
			printf("Corrected Time: from %lld to %lld (diff: %lld)\n", superframeStartTime, receivedSuperFrameStartTime, receivedSuperFrameStartTime - superframeStartTime);
			superframeStartTime = receivedSuperFrameStartTime;
		}

	} else if (buf[0] == 'D') {
		// Empfangenes Datagram ist Slot Message, ignorieren.
	} else {
		printf("### Unknown Message: '%s'\n", buf);
	}

	return beaconReceived;
}

void awaitTimer(sigset_t * sigset) {

	siginfo_t info;
	do {
		if (sigwaitinfo(sigset, &info) < 0) {
			perror("sigwait");
			exit(1);
		}

		switch (info.si_signo) {
			case SIGALRM:
				break;
			case SIGINT:
				//Cntrl-C wurde gedrueckt.
				//Programm beenden.
				exit(0);
				break;
				//default:
				//printf("Received Signal %d, ignoring\n", info.si_signo);
		}
	} while (info.si_signo != SIGALRM);
}

void sendBeacon() {
	uint64_t beaconDelay = now() - superframeStartTime;
	encodeBeacon(buf, sizeof(buf), frameCounter, beaconDelay, hostname);
	sendMessage(socketId, buf, mcastAddress, port);
	//printf("Sent beacon: %s\n", buf);
}

void sendSlotMessage() {
	encodeSlotMessage(buf, sizeof(buf), slot, hostname);
	sendMessage(socketId, buf, mcastAddress, port);
	//printf("Sent slot message (%s:%d): %s\n", mcastAddress, port, buf);
}







