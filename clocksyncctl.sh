#/bin/sh

# usage:
# clocksyncctl start [number] [address] [port] 
# clocksyncctl kill
set -e 

case $1 in
	start)
		if [[ $2 -gt 16 ]] ; then
			echo "Invalid number of processes"
			exit -1
		fi
		for i in $(seq 1 $2); do
			clocksync/clocksync -a $3 -p $4 -s $i -h "localhost-$i" 2> /dev/null 1>&2 &
		done
		;;
	kill)
		killall clocksync
		;;
	*)
		echo "Invalid parameters"
esac
